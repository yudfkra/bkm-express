<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use App\Libraries\Broker;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('site.layouts.header', 'App\Libraries\ProfileComposer');
        View::composer(['site.pages.branch.modal'], 'App\Http\ViewComposers\AgentViewComposer');
        View::composer(['site.pages.transaction.service_modal'], 'App\Http\ViewComposers\TransactionViewComposer@services');
        View::composer(['site.pages.transaction.delivery','site.pages.transaction.delivery_detail'], 'App\Http\ViewComposers\TransactionViewComposer@deliveryStatus');
        View::composer(['site.pages.transaction.delivery', 'site.pages.transaction.delivery_form'], 'App\Http\ViewComposers\TransactionViewComposer');

        View::composer('site.pages.transaction.delivery_service_input', 'App\Http\ViewComposers\TransactionViewComposer@delivery_data');
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
