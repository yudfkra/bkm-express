<?php

namespace App\Libraries;

use Illuminate\View\View;
use App\Libraries\Broker;

class ProfileComposer
{
    protected $broker;

    public function __construct(Broker $broker)
    {
        $this->broker = $broker;
    }

    public function compose(View $view)
    {
        $view->with([
            'broker_profile' => request()->auth_user,
            'broker_profile_url' => $this->broker->getServerURL(config('bkm.uri.profile')),
            'broker_logout_url' => $this->broker->getServerURL(config('bkm.uri.logout'), ['follow' => true]),
        ]);
    }

}
