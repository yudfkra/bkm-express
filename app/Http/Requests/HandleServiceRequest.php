<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HandleServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'status' => 'required|in:0,1',
            'price' => 'required|numeric',
        ];
    }

    public function attributes()
    {
        return [
            'name' => trans('page/transaction.service.field.name'),
            'status' => trans('page/transaction.service.field.status.title'),
            'price' => trans('page/transaction.service.field.price'),
        ];
    }

    public function withValidator($validator)
    {
        if ($validator->fails() && $this->method() == 'PUT') {
            $this->session()->flash('service_id', $this->segment(4));
        }
    }
}
