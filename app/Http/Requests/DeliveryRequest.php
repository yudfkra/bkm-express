<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DeliveryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'branch' => 'required|exists:branches,id',
            'bill_name' => 'required|string',
            'bill_address' => 'required|string',
            'bill_phone' => 'required|string',
            'reciever_name' => 'required|string',
            'reciever_address' => 'required|string',
            'reciever_phone' => 'required|string',
            'due_date' => 'required|date|date_format:Y-m-d|after:today',
            'details.*.service' => 'required',
            'details.*.description' => 'required|string',
            'details.*.qty' => 'required|numeric',
            'details.*.weight' => 'required|numeric',
        ];
    }

    public function attributes()
    {
        return [
            'branch' => trans('page/transaction.delivery.field.branch'),
            'bill_name' => trans('page/transaction.delivery.field.bill_name'),
            'bill_address' => trans('page/transaction.delivery.field.bill_address'),
            'bill_phone' => trans('page/transaction.delivery.field.bill_phone'),
            'reciever_name' => trans('page/transaction.delivery.field.reciever_name'),
            'reciever_address' => trans('page/transaction.delivery.field.reciever_address'),
            'reciever_phone' => trans('page/transaction.delivery.field.reciever_phone'),
            'due_date' => trans('page/transaction.delivery.field.due_date'),
            'details.*.service' => trans('page/transaction.delivery.field.detail.service'),
            'details.*.description' => trans('page/transaction.delivery.field.detail.description'),
            'details.*.qty' => trans('page/transaction.delivery.field.detail.qty'),
            'details.*.weight' => trans('page/transaction.delivery.field.detail.weight'),
        ];
    }
}
