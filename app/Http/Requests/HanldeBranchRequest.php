<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HanldeBranchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string',
            'agent_id' => 'required|exists:agents,id',
            'address' => 'required|string',
            'phone' => 'required|string',
            'fax' => 'required|numeric',
            'contact' => 'required|string',
            'cellular' => 'required|string',
        ];
    }

    public function attributes()
    {
        return [
            'name' => trans('page/branch.field.name'),
            'agent_id' => trans('page/branch.field.agent'),
            'address' => trans('page/branch.field.address'),
            'phone' => trans('page/branch.field.phone'),
            'fax' => trans('page/branch.field.fax'),
            'contact' => trans('page/branch.field.contact'),
            'cellular' => trans('page/branch.field.cellular'),
        ];
    }
}
