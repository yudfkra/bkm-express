<?php

namespace App\Http\Controllers;

use App\Models\Service;
use App\Models\Delivery;
use Illuminate\Http\Request;
use App\Http\Requests\DeliveryRequest;
use App\Http\Requests\HandleServiceRequest;
use Illuminate\Support\Facades\DB;

class TransactionController extends Controller
{
    protected $delivery_action = ['success', 'cancelled'];

    public function index(Request $request)
    {
        $deliveries = Delivery::withCount('details')->latest();

        $offset = (int) $request->input('page', 1);
        $offset = $offset > 1 ? ($offset - 1) * 15 : 0;

        $params = $request->only(['status', 'branch', 'keyword']);

        $data['delivery_action'] = $this->delivery_action;

        $data['offset'] = $offset + 1;
        $data['params'] = $params;
        $data['deliveries'] = $deliveries->paginate()->appends($params);
        $data['title'] = trans('page/transaction.delivery.title');
        // return $data;
        return view('site.pages.transaction.delivery', $data);
    }

    public function showDeliveryForm()
    {
        $data['title'] = trans('page/transaction.delivery.section.add');
        return view('site.pages.transaction.delivery_form', $data);
    }

    public function storeDelivery(DeliveryRequest $request)
    {
        $created = DB::transaction(function() use($request){
            $delivery = Delivery::create([
                'branch_id' => $request->input('branch'),
                'user_id' => null,
                'bill_name' => $request->input('bill_name'),
                'bill_address' => $request->input('bill_address'),
                'bill_phone' => $request->input('bill_phone'),
                'reciever_name' => $request->input('reciever_name'),
                'reciever_address' => $request->input('reciever_address'),
                'reciever_phone' => $request->input('reciever_phone'),
                'due_date' => \Carbon\Carbon::parse($request->input('due_date')),
                'status' => Delivery::DELIV_STATUS['process'],
            ]);
            $delivery->details()->createMany($request->input('details'));
            $delivery->update([
                'receipt' => $delivery->getReceiptFormat(),
                'total' => (int) str_replace('.', '', $request->input('total')),
            ]);
            return $delivery;
        });
        if($created){
            $request->session()->flash('flash_messages', ['success' => trans('messages.success.delivery.created')]);
            return redirect()->route('transaction.show', ['id' => $created->id]);
        }else{
            $request->session()->flash('flash_messages', ['errors' => trans('messages.errors.delivery.failed_create')]);
            return redirect()->back()->withInput();
        }
    }

    public function showDelivery($id)
    {
        $delivery = Delivery::with([
            'branch:id,agent_id,name,address,phone,fax',
            'branch.agent:id,name',
            'details'
        ])->find($id);

        $data['delivery_action'] = $this->delivery_action;

        $data['delivery'] = $delivery;
        $data['title'] = trans_choice('page/transaction.delivery.section.detail', !empty($delivery), [
            'invoice' => $delivery ? $delivery->receipt : null
        ]);
        // return $data;
        return view('site.pages.transaction.delivery_detail', $data);
    }

    public function handleDeliveryAction(Request $request, $id)
    {
        $delivery = Delivery::with([
            'branch:id,agent_id,name,address,phone,fax',
            'branch.agent:id,name',
        ])->find($id);

        if (!empty($delivery)) {
            $this->validate($request, [
                'action' => 'required|string|in:' . implode(',', $this->delivery_action),
            ]);
            $messages = [];
            $update = [];

            if ($delivery->status == Delivery::DELIV_STATUS['process']) {
                $update['status'] = Delivery::DELIV_STATUS[$request->input('action')];
                if ($delivery->update($update)) {
                    $messages['success'][] = trans('messages.success.delivery.updated', ['invoice' => $delivery->receipt]);
                }
            }else{
                $messages['errors'][] = trans('messages.errors.delivery.already_' . $request->input('action'));
            }
            $request->session()->flash('flash_messages', $messages);
        }else{
            $request->session()->flash('flash_messages', [
                'errors' => trans('messages.errors.not_found', true, ['item' => trans('page/transaction.delivery.title')])
            ]);
        }
        return redirect()->back();
    }

    public function printDelivery($id)
    {
        $delivery = Delivery::with([
            'branch:id,agent_id,name,address,phone,fax',
            'branch.agent:id,name',
            'details'
        ])->find($id);

        if (!empty($delivery)) {
            return view('site.pages.transaction.delivery_print',compact('delivery'));
        }

        request()->session()->flash('flash_messages', [
            'errors' => trans('messages.errors.not_found', true, ['item' => trans('page/transaction.delivery.title')])
        ]);
        return redirect()->route('transaction.index');
    }

    public function showServices(Request $request)
    {
        $services = Service::latest();

        $offset = (int) $request->input('page', 1);
        $offset = $offset > 1 ? ($offset - 1) * 15 : 0;

        // check jika sebelumnya merequest update dari validasi gagal.
        $servicedata = Service::find(session('service_id'));

        $data['offset'] = $offset + 1;
        $data['servicedata'] = $servicedata;
        $data['services'] = $services->paginate()->appends([]);
        $data['title'] = trans('page/transaction.service.title');
        return view('site.pages.transaction.services', $data);
    }

    public function storeService(HandleServiceRequest $request)
    {
        $service = new Service($request->only([
            'name', 'price', 'status'
        ]));
        if ($service->save()) {
            $request->session()->flash('flash_messages', ['success' => trans('messages.success.service.created')]);
            return redirect()->route('transaction.service.index');
        }else{
            $request->session()->flash('flash_messages', ['errors' => trans('messages.success.service.failed_create')]);
        }
        return redirect()->back();
    }

    public function updateService(HandleServiceRequest $request, $id)
    {
        $service = Service::find($id);
        if (!empty($service)) {
            $saved = $service->update($request->only([
                'name', 'price', 'status'
            ]));
            if ($saved) {
                $request->session()->flash('flash_messages', ['success' => trans('messages.success.service.updated')]);
                return redirect()->route('transaction.service.index');
            }else{
                $request->session()->flash('flash_messages', ['errors' => trans('messages.success.service.failed_update')]);
            }
        }else{
            $request->session()->flash('flash_messages', [
                'errors' => trans('messages.errors.not_found', ['item' => trans('page/transaction.service.title')])
            ]);
            return redirect()->back();
        }
    }

    public function destroyService(Request $request, $id)
    {
        $messages = [];
        $service = Service::find($id);
        if (!empty($service)) {
            if ($service->delete()) {
                $messages['success'][] = trans('messages.success.service.deleted');
            }else{
                $messages['errors'][] = trans('messages.errors.service.failed_delete');
            }
        }else{
            $messages['errors'][] = trans('messages.errors.not_found', [
                'item' => trans('page/transaction.service.title')
            ]);
        }
        $request->session()->flash('flash_messages', $messages);
        return redirect()->back();
    }
}
