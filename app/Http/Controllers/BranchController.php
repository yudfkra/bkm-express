<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\HanldeBranchRequest;
use App\Models\Branch;

class BranchController extends Controller
{
    public function showBranchList(Request $request)
    {
        $branches = Branch::with('agent')->latest();

        $offset = (int) $request->input('page', 1);
        $offset = $offset > 1 ? ($offset - 1) * 15 : 0;

        $data['offset'] = $offset + 1;
        $data['branches'] = $branches->paginate()->appends([]);
        $data['title'] = trans('page/branch.title');
        return view('site.pages.branch.index', $data);
    }

    public function storeBranch(HanldeBranchRequest $request)
    {
        $messages = [];
        $added = DB::transaction(function () use($request) {
            $branch = Branch::create([
                'agent_id' => $request->input('agent_id'),
                'user_id' => null, //$request->auth_user['id'],
                'name' => $request->input('name'),
                'address' => $request->input('address'),
                'phone' => $request->input('phone'),
                'fax' => $request->input('fax'),
                'contact' => $request->input('contact'),
                'cellular' => $request->input('cellular'),
            ]);
            return $branch;
        });
        if ($added) {
            $messages['success'][] = trans('messages.success.branch.created');
        }else{
            $messages['errors'][] = trans('messages.errors.branch.failed_create');
        }
        $request->session()->flash('flash_messages', $messages);

        return $added
            ? redirect()->route('branch.index')
            : redirect()->back()->withInput();
    }

    public function showBranch($id = '')
    {
        $branch = Branch::with('agent')->find($id);

        $data['branch'] = $branch;
        $data['title'] = trans_choice('page/branch.section.detail', !empty($branch), [
            'branch' => $branch ? $branch->name : null,
        ]);
        return view('site.pages.branch.detail', $data);
    }

    public function updateBranch(HanldeBranchRequest $request, $id)
    {
        $branch = Branch::find($id);
        if (!empty($branch)) {
            $messages = [];
            $updated = DB::transaction(function () use ($branch, $request) {
                return $branch->update([
                    'agent_id' => $request->input('agent_id'),
                    'user_id' => null, //$request->auth_user['id'],
                    'name' => $request->input('name'),
                    'address' => $request->input('address'),
                    'phone' => $request->input('phone'),
                    'fax' => $request->input('fax'),
                    'contact' => $request->input('contact'),
                    'cellular' => $request->input('cellular'),
                ]);
            });
            if ($updated) {
                $messages['success'][] = trans('messages.success.branch.updated');
            } else {
                $messages['errors'][] = trans('messages.errors.branch.failed_update');
            }
            $request->session()->flash('flash_messages', $messages);

            return $updated
                ? redirect()->route('branch.detail', ['id' => $branch->id])
                : redirect()->back()->withInput();   
        }else{
            $request->session()->flash('flash_messages', [
                'errors' => trans('messages.errors.not_found', ['item' => trans('page/branch.title')])
            ]);
            return redirect()->back();
        }
    }

    public function destroyBranch(Request $request, $id)
    {
        $branch = Branch::find($id);
        if (!empty($branch)) {
            if ($branch->delete()) {
                $request->session()->flash('flash_messages', [
                    'success' => trans('messages.success.branch.deleted')
                ]);
            }else{
                $request->session()->flash('flash_messages', [
                    'errors' => trans('messages.errors.branch.failed_delete')
                ]);
            }
        }else{
            $request->session()->flash('flash_messages', [
                'errors' => trans('messages.errors.not_found', [
                    'item' => trans('page/branch.title')
                ])
            ]);
        }
        return redirect()->back();
    }
}
