<?php

namespace App\Http\ViewComposers;

use App\Models\Agent;

class AgentViewComposer
{
    public function compose($view)
    {
        $view->with([
            'agents' => $this->getAgents(),
        ]);
    }

    public function getAgents()
    {
        return Agent::pluck('name', 'id');
    }
}
