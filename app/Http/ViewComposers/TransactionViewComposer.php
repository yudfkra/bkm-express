<?php

namespace App\Http\ViewComposers;

use App\Models\Service;
use App\Models\Delivery;
use App\Models\Branch;

class TransactionViewComposer
{
    public function compose($view)
    {
        $view->with([
            'branches' => $this->getPluckedBranch(),
        ]);
    }

    public function delivery_data($view)
    {
        $view->with([
            'services' => Service::orderBy('name', 'ASC')->get(),
        ]);
    }

    public function getPluckedBranch()
    {
        return Branch::pluck('name', 'id');
    }

    public function getPluckedService()
    {
        return Service::pluck('name', 'id');
    }

    public function services($view)
    {
        $view->with([
            'service_status' => $this->getServiceStatus(),
        ]);
    }

    public function deliveryStatus($view)
    {
        $view->with([
            'delivery_status' => Delivery::DELIV_STATUS,
        ]);
    }

    public function getDeliveryStatus()
    {
        $status = [];
        foreach (Delivery::DELIV_STATUS as $key => $value) {
            $status[$value] = trans('page/transaction.delivery.field.status.' . $key);
        }
        return $status;
    }

    public function getServiceStatus()
    {
        $status = [];
        foreach (Service::STATUS as $key => $det) {
            $status[$det] = trans('page/transaction.service.field.status.' . $key);
        }
        return $status;
    }
}
