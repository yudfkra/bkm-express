<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Agent extends Model
{
    protected $table = 'agents';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name',
    ];

    public function branches()
    {
        return $this->hasMany(Branch::class, 'agent_id');
    }
}
