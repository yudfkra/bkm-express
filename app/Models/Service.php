<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    protected $table = 'services';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'status', 'price',
    ];

    protected $appends = [
        'fullname_display',
    ];

    const STATUS = [
        'inactive' => 0,
        'active' => 1,
    ];

    public function getStatusDisplayAttribute()
    {
        return array_search($this->attributes['status'], static::STATUS);
    }

    public function getFullnameDisplayAttribute()
    {
        return "{$this->attributes['name']} - " . formatRp($this->attributes['price']);
    }
}
