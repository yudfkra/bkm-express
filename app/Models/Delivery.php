<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Delivery extends Model
{
    protected $table = 'deliveries';

    protected $primaryKey = 'id';

    protected $fillable = [
        'branch_id', 'user_id', 'receipt', 'bill_name', 'bill_address', 'bill_phone',
        'reciever_name', 'reciever_address', 'reciever_phone', 'total', 'status',
        'due_date',
    ];

    protected $dates = ['due_date'];

    const DELIV_STATUS = [
        'cancelled' => 0,
        'success' => 1,
        'process' => 2,
    ];

    public function getReceiptFormat()
    {
        return sprintf('#%s/%05d', Carbon::parse($this->attributes['created_at'])->format('Y/m/d'), $this->attributes['id']);
    }

    public function getStatusDisplayAttribute()
    {
        return array_flip(static::DELIV_STATUS)[$this->attributes['status']];
    }

    public function getLabelStatusDisplayAttribute()
    {
        switch ($this->attributes['status']) {
            case static::DELIV_STATUS['success']:
                $label = 'success';
                break;
            case static::DELIV_STATUS['process']:
                $label = 'info';
                break;
            default: $label = 'danger'; break;
        }
        return $label;
    }

    public function details()
    {
        return $this->hasMany(DeliveryDetail::class, 'delivery_id');
    }

    public function branch()
    {
        return $this->belongsTo(Branch::class, 'branch_id');
    }
}
