<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryDetail extends Model
{
    protected $table = 'delivery_details';

    protected $primaryKey = 'id';

    protected $fillable = [
        'delivery_id', 'service_id', 'description',
        'qty', 'weight', 'price',
    ];

    public function delivery()
    {
        return $this->belongsTo(Delivery::class,  'delivery_id');
    }

    public function service()
    {
        return $this->belongsTo(Service::class, 'service_id');
    }
}
