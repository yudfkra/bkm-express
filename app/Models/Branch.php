<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Branch extends Model
{
    use SoftDeletes;

    protected $table = 'branches';

    protected $primaryKey = 'id';

    protected $fillable = [
        'agent_id', 'user_id', 'name', 'address', 'phone',
        'fax', 'contact', 'cellular',
    ];

    protected $dates = ['deleted_at'];

    public function agent()
    {
        return $this->belongsTo(Agent::class, 'agent_id');
    }

    public function deliveries()
    {
        return $this->hasMany(Delivery::class, 'branch_id');
    }
}
