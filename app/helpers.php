<?php

if (!function_exists('formatRp')) {
    function formatRp($nominal = 0, $format = "Rp", $symbol = true)
    {
        $result = number_format($nominal, 0, ',', '.');
        return ($symbol) ? $format . " " . $result : $result;
    }
}

if (!function_exists('format_currency')) {
    function format_currency($nominal = 0, $currency = "IDR", $symbol = false)
    {
        if ($currency == "IDR" || $currency == 'Rp') {
            return formatRp($nominal, $currency, $symbol);
        }
        return ($symbol) ? $currency . " " . $nominal : $nominal;
    }
}