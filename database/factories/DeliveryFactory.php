<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Delivery::class, function (Faker $faker) {
    return [
        'receipt' => null,
        'bill_name' => $faker->name,
        'bill_address' => $faker->address,
        'bill_phone' => $faker->phoneNumber,
        'reciever_name' => $faker->name,
        'reciever_address' => $faker->address,
        'reciever_phone' => $faker->phoneNumber,
        'total' => 0,
        'status' => $faker->numberBetween(0, 2),
        'due_date' => $faker->dateTimeBetween('now', '+ 14 days', 'Asia/Jakarta'),
    ];
});

$factory->state(App\Models\Delivery::class, 'cancelled', [
    'status' => App\Models\Delivery::DELIV_STATUS['cancelled']
]);

$factory->state(App\Models\Delivery::class, 'success', [
    'status' => App\Models\Delivery::DELIV_STATUS['success']
]);

$factory->state(App\Models\Delivery::class, 'process', [
    'status' => App\Models\Delivery::DELIV_STATUS['process']
]);
