<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Branch::class, function (Faker $faker) {
    return [
        'name' => $faker->company,
        'address' => $faker->address,
        'phone' => $faker->tollFreePhoneNumber,
        'fax' => $faker->randomNumber(7),
        'contact' => $faker->name,
        'cellular' => $faker->phoneNumber,
    ];
});
