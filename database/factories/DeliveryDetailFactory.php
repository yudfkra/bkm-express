<?php

use Faker\Generator as Faker;

$factory->define(App\Models\DeliveryDetail::class, function (Faker $faker) {
    $qty = $faker->randomDigitNotNull;
    $weight = $faker->numberBetween(1000, 2000);
    $price = $faker->numberBetween(10000, 100000);
    return [
        'description' => $faker->realText(30, 1),
        'qty' => $qty,
        'weight' => $weight,
        'price' => $price,
    ];
});
