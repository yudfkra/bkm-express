<?php

use Illuminate\Database\Seeder;

class BranchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agents = App\Models\Agent::all();
        foreach ($agents as $agent) {
            $agent->branches()->saveMany(factory(App\Models\Branch::class, rand(1, 20))->make());
        }
    }
}
