<?php

use Illuminate\Database\Seeder;

class AgentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $agents = App\Models\Agent::insert([
            ['name' => 'Kantor Perwakilan'],
            ['name' => 'Agen Utama'],
            ['name' => 'Sub Agen'],
        ]);
    }
}
