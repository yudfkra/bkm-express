<?php

use Illuminate\Database\Seeder;

class DeliverySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach (App\Models\Delivery::DELIV_STATUS as $key => $value) {
            $deliveries = factory(App\Models\Delivery::class, rand(1, 10))->states($key)->create();
            $deliveries->each(function ($delivery) {
                $details = factory(App\Models\DeliveryDetail::class, rand(1, 4))->make([
                    'service_id' => App\Models\Service::inRandomOrder()->first()->id,
                ]);
                $delivery->details()->saveMany($details);
                $delivery->update([
                    'receipt' => $delivery->getReceiptFormat(),
                    'branch_id' => App\Models\Branch::inRandomOrder()->first()->id,
                    'total' => $details->sum(function ($detail) {
                        return $detail->qty * $detail->price;
                    }),
                ]);
            });
        }
    }
}
