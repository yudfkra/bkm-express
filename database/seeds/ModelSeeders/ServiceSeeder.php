<?php

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\Service::class)->create([
            'name' => 'BKM Fast',
        ]);
        factory(App\Models\Service::class)->create([
            'name' => 'BKM Medium',
        ]);
        factory(App\Models\Service::class)->create([
            'name' => 'BKM Slow',
        ]);
    }
}
