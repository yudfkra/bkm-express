<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeliveriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deliveries', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('branch_id')->unsigned()->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('receipt')->nullable();
            $table->string('bill_name', 50);
            $table->text('bill_address');
            $table->string('bill_phone', 30);
            $table->string('reciever_name', 50);
            $table->text('reciever_address');
            $table->string('reciever_phone', 30);
            $table->double('total')->default(0);
            $table->tinyInteger('status')->comment('0 = Batal;1 = Success;2 = Process;');
            $table->dateTime('due_date')->nullable();
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deliveries');
    }
}
