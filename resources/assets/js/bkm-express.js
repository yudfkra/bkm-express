window.initPixel = [];

function serviceFormHandler(service, type) {
    console.log(service);
    switch (type) {
        case 'edit':
            $('#form-modal #input-name').val(service.name);
            $('#form-modal #input-price').val(service.price);
            $('#form-modal #input-status').val(service.status);
            break;
        default:
            $('#form-modal #input-name').val('');
            $('#form-modal #input-price').val('');
            $('#form-modal #input-status').val('');
            break;
    }
}

window.initPixel.push(function(){
    $('.select2').select2();

    $(document).on("focus", ".form-control", function () {
        var input = $(this);
        $('.form-group[data-id="' + input.attr("id") + '"]').removeClass("has-error");
    });

    $(document).on("blur", ".form-control", function () {
        var input = $(this);
        if (input.val()) {
            $('.form-group[data-id="' + input.attr("id") + '"]').removeClass("has-error");
            $('.help-block[data-id="' + input.attr("id") + '"]').hide();
        } else {
            $('.form-group[data-id="' + input.attr("id") + '"]').addClass("has-error");
            $('.help-block[data-id="' + input.attr("id") + '"]').show();
        }
    });

    $(document).on('click', '.btn-loading', function(){
        const btn = $(this);
        btn.button('loading');
    });

    $('.confirm-modal').on('click', function (e) {
        const btn = $(this);
        bootbox.confirm({
            // 'Anda yakin ingin menghapus Data ini ?'
            message: btn.data('message'),
            callback: function (result) {
                if (result) {
                    btn.parent().submit();
                }
            },
            className: "bootbox-sm",
        });
    });

    $('.handler-modal').on('click', function(){
        const elem = $(this);
        type = elem.data("type");
        section = elem.data("section");
        valuedata = elem.data("valuedata");
        url = elem.data('url');

        $('#form-modal #modalHeading').text(elem.data('title'));
        $('#form-modal #form-section').attr('action', url);
        if (type == "add") {
            $('#form-modal input[name="_method"]').remove();
        }
        if (type == "edit") {
            $('#form-modal #form-section').prepend(function () {
                return '<input type="hidden" name="_method" value="PUT" />';
            });
        }
        switch (section) {
            case 'service':
                serviceFormHandler(valuedata, type);
                break;
            default:
                break;
        }
    });
});