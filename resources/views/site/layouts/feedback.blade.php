@if (session('flash_messages'))
    @foreach (session('flash_messages') as $type => $messages)
        @php
            $_messages = !is_array($messages) ? [$messages] : $messages;
        @endphp
        @foreach ($_messages as $message)
            <div class="alert fade in alert-{{ config('site.flash-message.' . $type, 'info') }}">
                <button type="button" class="close" data-dismiss="alert">×</button>
                <h4>{{ trans('messages.' . $type . '.title') }}</h4>
                {!! $message !!}
            </div>
        @endforeach
    @endforeach
@endif