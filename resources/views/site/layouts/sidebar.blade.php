<div id="main-menu-inner">
    <ul class="navigation">
        <li class="{{ empty(Request::segment(1)) ? 'active' : '' }}">
            <a href="{{ route('index') }}">
                <i class="menu-icon fa fa-dashboard"></i>
                <span class="mm-text">@lang('layout.menu.dashboard.title')</span>
            </a>
        </li>
        <li class="{{ (Request::segment(1) == 'branch') ? 'active' : '' }}">
            <a href="{{ route('branch.index') }}">
                <i class="menu-icon fa fa-building-o"></i>
                <span class="mm-text">@lang('layout.menu.branch.title')</span>
            </a>
        </li>
        <li class="mm-dropdown {{ Request::segment(1) == 'transaction' ? 'active open' : '' }}">
            <a href="#">
                <i class="menu-icon fa fa-inbox"></i>
                <span class="mm-text">@lang('layout.menu.transaction.title')</span>
            </a>
            <ul>
                <li class="{{ (Request::segment(1) == 'transaction' && (empty(Request::segment(2)) || Request::segment(2) != 'service')) ? 'active' : '' }}">
                    <a href="{{ route('transaction.index') }}">
                        <i class="menu-icon fa fa-truck"></i>
                        <span class="mm-text">@lang('layout.menu.transaction.child.delivery')</span>
                    </a>
                </li>
                <li class="{{ (Request::segment(1) == 'transaction' && Request::segment(2) == 'service') ? 'active' : '' }}">
                    <a href="{{ route('transaction.service.index') }}">
                        <i class="menu-icon fa fa-suitcase"></i>
                        <span class="mm-text">@lang('layout.menu.transaction.child.service')</span>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>