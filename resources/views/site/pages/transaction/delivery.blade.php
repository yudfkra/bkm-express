@extends('site.layouts.app')
@section('content')
    @component('site.layouts.page_header')
        @slot('title') {{ $title }} @endslot
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('transaction.create') }}">
                <i class="fa fa-plus"></i> @lang('page/transaction.delivery.section.add')
            </a>
        </div>
    @endcomponent
    @includeWhen(session('flash_messages'), 'site.layouts.feedback')
    <div class="panel-group" id="filter-group">
        <div class="panel">
            <div class="panel-heading">
                <a class="accordion-toggle {{ !empty($params) ? 'collapsed' : 'collapse' }}" data-toggle="collapse" href="#collapseFilter" data-parent="#filter-group">@lang('page/transaction.delivery.section.filter')</a>
            </div>
            <div id="collapseFilter" class="panel-collapse collapse {{ !empty($params) ? 'in' : '' }}">
                <div class="panel-body">
                </div>
            </div>
        </div>
    </div>
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">@lang('page/transaction.delivery.description')</span>
        </div>
        <div class="panel-body with-margins">
            <div class="table-primary table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 10px;">#</th>
                            <th style="width: 140px;">@lang('page/transaction.delivery.field.invoice')</th>
                            <th>@lang('page/transaction.delivery.field.bill_name')</th>
                            <th>@lang('page/transaction.delivery.field.reciever_name')</th>
                            <th style="width: 130px;">@lang('page/transaction.delivery.field.total')</th>
                            <th>@lang('page/transaction.delivery.field.status.title')</th>
                            <th style="width: 140px;">@lang('page/transaction.delivery.field.created_at')</th>
                            <th style="width: 140px;">@lang('page/transaction.delivery.field.due_date')</th>
                            <th style="width: 100px;">@lang('layout.text.action')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @forelse ($deliveries as $delivery)
                            <tr>
                                <td>{{ $offset++ }}</td>
                                <td>
                                    <a href="{{ route('transaction.show', ['id' => $delivery->id]) }}">{{ $delivery->receipt }}</a>
                                </td>
                                <td>{{ $delivery->bill_name }}</td>
                                <td>{{ $delivery->reciever_name }}</td>
                                <td>{{ formatRp($delivery->total) }}</td>
                                <td class="text-center">
                                    <span class="label label-{{ $delivery->label_status_display }}">{{ trans('page/transaction.delivery.field.status.' . $delivery->status_display) }}</span>
                                </td>
                                <td>{{ $delivery->created_at->format('H:i - d F Y') }}</td>
                                <td>{{ $delivery->due_date->format('H:i - d F Y') }}</td>
                                <td class="text-center">
                                    <a href="{{ route('transaction.show', ['id' => $delivery->id]) }}" class="btn btn-primary" data-toggle="tooltip" title="{{ trans('page/transaction.delivery.section.detail') }}" data-placement="top"><i class="fa fa-search"></i></a>
                                    @if ($delivery->status == $delivery_status['process'])
                                        @foreach ($delivery_action as $action)
                                            {{ Form::open([
                                                'route' => ['transaction.action', $delivery->id],
                                                'method' => 'patch',
                                                'style' => 'display: inline;'
                                            ]) }}
                                                <input type="hidden" name="action" value="{{ $action }}">
                                                <button type="button" class="btn btn-{{ $action == 'success' ? 'success' : 'danger' }} confirm-modal" data-message="{{ trans('page/transaction.delivery.text.confirm_' . $action) }}">
                                                    <i class="fa fa-{{ $action == 'success' ? 'check' : 'times' }}"></i>
                                                </button>
                                            {{ Form::close() }}
                                        @endforeach
                                    @endif
                                </td>
                            </tr>
                        @empty
                            <tr>
                                <td colspan="9" align="center">{{ trans('messages.errors.empty_data') }}</td>
                            </tr>
                        @endforelse
                    </tbody>
                </table>
            </div>
            <div class="pull-right">
                {!! $deliveries->links() !!}
            </div>
        </div>
    </div>
@endsection