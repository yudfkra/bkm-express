<!DOCTYPE html>
<!--[if IE 8]>         <html class="ie8" lang="{{ app()->getLocale() }}"> <![endif]-->
<!--[if IE 9]>         <html class="ie9 gt-ie8" lang="{{ app()->getLocale() }}"> <![endif]-->
<!--[if gt IE 9]><!-->
<html class="gt-ie8 gt-ie9 not-ie" lang="{{ app()->getLocale() }}">
<!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0">
    <title>{{ trans_choice('layout.meta.title', !empty($title), ['appname' => config('app.name'), 'title' => $title ?? '']) }}</title>
    <!-- Open Sans font from Google CDN -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,600,700,300&subset=latin"
        rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/pixel-admin.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/widgets.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/pages.min.css') }}">
    <link rel="stylesheet" href="{{ asset('pixel-admin/css/themes.min.css') }}">
    <!--[if lt IE 9]>
            <script src="assets/javascripts/ie.min.js"></script>
        <![endif]-->
    @stack('styles')
</head>
<body class="page-invoice page-invoice-print">
    <div class="invoice">
        <div class="invoice-header">
            <h3>
                <div class="invoice-logo demo-logo"><img src="{{ asset('assets/logo.png') }}" alt="" style="width:100%;height:100%;"></div>
                <div>
                    <small><strong>{{ config('app.name') }}</strong></small><br>
                    {{ trans('page/transaction.delivery.field.invoice').' '.$delivery->receipt }}
                </div>
                <div>
                    <span class="label label-{{ $delivery->label_status_display }}">{{ trans('page/transaction.delivery.field.status.' . $delivery->status_display) }}</span>
                </div>
            </h3>
            <address>
                {{ $delivery->branch->name }}<br>
                {!! nl2br($delivery->branch->address) !!}<br>
                {{ $delivery->branch->phone }}
            </address>
            <div class="invoice-date" style="margin-left: 10rem;">
                <small><strong>@lang('page/transaction.delivery.field.due_date')</strong></small><br>
                {{ $delivery->due_date->format('d F Y') }}
            </div>
            <div class="invoice-date">
                <small><strong>@lang('page/transaction.delivery.field.created_at')</strong></small><br>
                {{ $delivery->created_at->format('H:i - d F Y') }}
            </div>
        </div>
        <div class="invoice-info">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-md-6">
                        <table>
                            <thead>
                                <tr><th>@lang('page/transaction.delivery.field.bill_name')</th></tr>
                            </thead>
                            <tbody>
                                <tr><td><strong>{{ $delivery->bill_name }}</strong></td></tr>
                                <tr><td>{!! nl2br($delivery->bill_address) !!}</td></tr>
                                <tr><td>{{ $delivery->bill_phone }}</td></tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-6">
                        <table>
                            <thead>
                                <tr><th>@lang('page/transaction.delivery.field.reciever_name')</th></tr>
                            </thead>
                            <tbody>
                                <tr><td><strong>{{ $delivery->reciever_name }}</strong></td></tr>
                                <tr><td>{!! nl2br($delivery->reciever_address) !!}</td></tr>
                                <tr><td>{{ $delivery->reciever_phone }}</td></tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <hr>
        <div class="invoice-table">
            <table>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>@lang('page/transaction.delivery.field.detail.description')</th>
                        <th class="text-right">@lang('page/transaction.delivery.field.detail.qty')</th>
                        <th class="text-right">@lang('page/transaction.delivery.field.detail.weight')</th>
                        <th class="text-right">@lang('page/transaction.delivery.field.detail.price')</th>
                        <th class="text-right">@lang('page/transaction.delivery.field.detail.subtotal')</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($delivery->details as $detail)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>
                                {{ nl2br($detail->description) }}
                                {{-- <div class="invoice-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</div> --}}
                            </td>
                            <td class="text-right">{{ $detail->qty }}</td>
                            <td class="text-right">{{ $detail->weight }}</td>
                            <td class="text-right">{{ formatRp($detail->price) }}</td>
                            <td class="text-right">{{ formatRp($detail->qty * $detail->price) }}</td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <hr>
        <div class="invoice-info">
            <div class="invoice-total">
                <span>{{ formatRp($delivery->total) }}</span>
                @lang('page/transaction.delivery.field.total') :
            </div>
        </div>
    </div>
    <script type="text/javascript">
        window.onload = function () {
            window.print();
        }
    </script>
</body>
</html>