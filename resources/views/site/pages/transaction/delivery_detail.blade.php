@extends('site.layouts.app')
@section('content')
    @component('site.layouts.page_header')
        @slot('title') {{ $title }} @endslot
        @if (!empty($delivery))
            <div class="pull-right">
                <a href="{{ url()->previous() != url()->current() ? url()->previous() : route('transaction.index') }}" class="btn btn-default">@lang('layout.buttons.back')</a>
                @if ($delivery->status == $delivery_status['process'])
                    @foreach ($delivery_action as $action)
                        {{ Form::open([
                            'route' => ['transaction.action', $delivery->id],
                            'method' => 'patch',
                            'style' => 'display: inline;'
                        ]) }}
                            <input type="hidden" name="action" value="{{ $action }}">
                            <button type="button" class="btn btn-{{ $action == 'success' ? 'success' : 'danger' }} confirm-modal" data-message="{{ trans('page/transaction.delivery.text.confirm_' . $action) }}">
                                <i class="fa fa-{{ $action == 'success' ? 'check' : 'times' }}"></i> @lang('page/transaction.delivery.buttons.' . $action)
                            </button>
                        {{ Form::close() }}
                    @endforeach
                @endif
                <a href="{{ route('transaction.print', ['id' => $delivery->id]) }}" class="btn btn-primary" target="_blank"><i class="fa fa-print"></i>&nbsp;&nbsp;@lang('page/transaction.delivery.text.print')</a>
            </div>
        @endif
    @endcomponent
    @includeWhen(session('flash_messages'), 'site.layouts.feedback')
    @if (!empty($delivery))
        <div class="panel invoice">
            <div class="invoice-header">
                <h3>
                    <div class="invoice-logo demo-logo"><img src="{{ asset('assets/logo.png') }}" alt="" style="width:100%;height:100%;"></div>
                    <div>
                        <small><strong>{{ config('app.name') }}</strong></small><br>
                        {{ trans('page/transaction.delivery.field.invoice').' '.$delivery->receipt }}
                    </div>
                    {{-- <div>
                        <span class="label label-{{ $delivery->label_status_display }}">{{ trans('page/transaction.delivery.field.status.' . $delivery->status_display) }}</span>
                    </div> --}}
                </h3>
                <address>
                    {{ $delivery->branch->name }}<br>
                    {!! nl2br($delivery->branch->address) !!}<br>
                    {{ $delivery->branch->phone }}
                </address>
                <div class="invoice-date">
                    <small><strong>@lang('page/transaction.delivery.field.due_date')</strong></small><br>
                    {{ $delivery->due_date->format('d F Y') }}
                </div>
                <div class="invoice-date" style="margin-right: 10rem;">
                    <small><strong>@lang('page/transaction.delivery.field.created_at')</strong></small><br>
                    {{ $delivery->created_at->format('H:i - d F Y') }}
                </div>
            </div>
            <div class="invoice-info">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-6">
                            <table>
                                <thead>
                                    <tr><th>@lang('page/transaction.delivery.field.bill_name')</th></tr>
                                </thead>
                                <tbody>
                                    <tr><td><strong>{{ $delivery->bill_name }}</strong></td></tr>
                                    <tr><td>{!! nl2br($delivery->bill_address) !!}</td></tr>
                                    <tr><td>{{ $delivery->bill_phone }}</td></tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table>
                                <thead>
                                    <tr><th>@lang('page/transaction.delivery.field.reciever_name')</th></tr>
                                </thead>
                                <tbody>
                                    <tr><td><strong>{{ $delivery->reciever_name }}</strong></td></tr>
                                    <tr><td>{!! nl2br($delivery->reciever_address) !!}</td></tr>
                                    <tr><td>{{ $delivery->reciever_phone }}</td></tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="invoice-table">
                <table>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>@lang('page/transaction.delivery.field.detail.description')</th>
                            <th class="text-right">@lang('page/transaction.delivery.field.detail.qty')</th>
                            <th class="text-right">@lang('page/transaction.delivery.field.detail.weight')</th>
                            <th class="text-right">@lang('page/transaction.delivery.field.detail.price')</th>
                            <th class="text-right">@lang('page/transaction.delivery.field.detail.subtotal')</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($delivery->details as $detail)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>
                                    {{ nl2br($detail->description) }}
                                    {{-- <div class="invoice-description">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.</div> --}}
                                </td>
                                <td class="text-right">{{ $detail->qty }}</td>
                                <td class="text-right">{{ $detail->weight }}</td>
                                <td class="text-right">{{ formatRp($detail->price) }}</td>
                                <td class="text-right">{{ formatRp($detail->qty * $detail->price) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <hr>
            <div class="invoice-info">
                <div class="invoice-total">
                    <span>{{ formatRp($delivery->total) }}</span>
                    @lang('page/transaction.delivery.field.total') :
                </div>
            </div>
        </div>
    @else
        @include('site.pages.notfound', ['route' => route('transaction.index')])
    @endif
@endsection