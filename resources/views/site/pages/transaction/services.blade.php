@extends('site.layouts.app')
@section('content')
    @component('site.layouts.page_header')
        @slot('title') {{ $title }} @endslot
        <div class="pull-right">
            <button class="btn btn-primary handler-modal" data-toggle="modal" data-target="#form-modal" data-type="add" data-url="{{ route("transaction.service.store") }}" data-title="@lang('page/transaction.service.section.add')" data-section="service">
                <i class="fa fa-plus"></i> @lang('page/transaction.service.section.add')
            </button>
        </div>
    @endcomponent
    @includeWhen(session('flash_messages'), 'site.layouts.feedback')
    <div class="row">
        <div class="col-md-9 col-xs-offset-1">
            <div class="panel">
                <div class="panel-heading">
                    <span class="panel-title">@lang('page/transaction.service.description')</span>
                </div>
                <div class="panel-body with-margins">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-primary">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th style="width: 10px;">#</th>
                                            <th>@lang('page/transaction.service.field.name')</th>
                                            <th>@lang('page/transaction.service.field.price')</th>
                                            <th style="width: 100px;">@lang('page/transaction.service.field.status.title')</th>
                                            <th style="width: 100px;">@lang('layout.text.action')</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @forelse ($services as $service)
                                            <tr>
                                                <td>{{ $offset++ }}</td>
                                                <td>{{ $service->name }}</td>
                                                <td>{{ format_currency($service->price) }}</td>
                                                <td class="text-center">
                                                    <span class="label label-{{ $service->status ? 'success' : 'danger' }}">{{ trans('page/transaction.service.field.status.' . $service->status_display) }}</span>
                                                </td>
                                                <td>
                                                    <button class="btn btn-primary handler-modal" data-toggle="modal" data-target="#form-modal" data-valuedata='{!! $service !!}' data-type="edit" data-url="{{ route('transaction.service.update', ['id' => $service->id]) }}" data-title="@lang('page/transaction.service.section.edit')" data-section="service"><i class="fa fa-pencil-square-o"></i></button>
                                                    {{ Form::open([
                                                        'method'=>'delete',
                                                        'route' => ['transaction.service.destroy', $service->id],
                                                        'style' => 'display:inline;'
                                                    ]) }}
                                                        <button type="button" class="btn btn-danger confirm-modal" data-message="{{ trans('page/transaction.service.text.confirm_delete') }}"><i class="fa fa-trash-o" data-toggle="tooltip" title="{{ trans_choice('common.buttons.delete', 0) }}" data-placement="top"></i></button>
                                                    {{ Form::close() }}
                                                </td>
                                            </tr>
                                        @empty
                                            <tr>
                                                <td colspan="5" align="center">{{ trans('messages.errors.empty_data') }}</td>
                                            </tr>
                                        @endforelse
                                    </tbody>
                                </table>
                            </div>
                            {!! $services->links() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('html')
    @include('site.pages.transaction.service_modal', [
        'route' => $servicedata ? ['transaction.service.update', $servicedata->id] : 'transaction.service.store',
        'method' => $servicedata ? 'PUT' : 'POST',
        'modalTitle' => $servicedata ? trans('page/transaction.service.section.edit') : trans('page/transaction.service.section.add'),
        'servicedata' => $servicedata,
    ])
@endpush