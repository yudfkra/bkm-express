@if (!empty($details) && empty($index))
    @foreach ($details as $detail)
        <tr id="detail-{{ $loop->index }}">
            <td>
                <div class="form-group {{ $errors->has('details.' . $loop->index . '.service') ? 'has-error simple' : '' }}" data-id="{{ 'input-service-' . $loop->index }}">
                    <div class="col-sm-12">
                        <select name="details[{{ $loop->index }}][service]" id="input-service-{{ $loop->index }}" class="form-control select-services">
                            <option disabled {{ empty(old('details.' . $loop->index . '.service')) ? 'selected' : '' }}>{{ trans('page/transaction.delivery.field.detail.service') }}</option>
                            @foreach ($services as $service)
                                <option value="{{ $service->id }}" {{ old('details.' . $loop->parent->index . '.service') == $service->id ? 'selected' : '' }} data-service='{!! json_encode($service) !!}'>{{ $service->fullname_display }}</option>
                            @endforeach
                        </select>
                        <input id="input-price-{{ $loop->index }}" type="hidden" name="details[{{ $loop->index }}][price]" value="{{ old('details.' . $loop->index . '.price') }}" readonly>
                        {!! $errors->has('details.' . $loop->index . '.service') ? ' <p class="help-block" data-id="input-service-" ' . $loop->index . '>'.$errors->first('details.' . $loop->index . '.service').'</p>' : "" !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group {{ $errors->has('details.' . $loop->index . '.description') ? 'has-error simple' : '' }}" data-id="{{ 'input-description-' . $loop->index }}">
                    <div class="col-sm-12">
                        <input id="{{ 'input-description-' . $loop->index }}" class="form-control" type="text" min="0" name="{{ 'details[' . $loop->index . '][description]' }}" placeholder="{{ trans('page/transaction.delivery.field.detail.description') }}" value="{{ old('details.' . $loop->index . '.description') }}">
                        {!! $errors->has('details.' . $loop->index . '.description') ? '<p class="help-block" data-id="input-description-" ' . $loop->index . '>'.$errors->first('details.' . $loop->index . '.description').'</p>' : "" !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group {{ $errors->has('details.' . $loop->index . '.weight') ? 'has-error simple' : '' }}" data-id="{{ 'input-weight-' . $loop->index }}">
                    <div class="col-sm-12">
                        <input id="{{ 'input-weight-' . $loop->index }}" class="form-control weight-services" type="number" min="0" step="50" name="{{ 'details[' . $loop->index . '][weight]' }}" placeholder="{{ trans('page/transaction.delivery.field.detail.weight') }}" value="{{ old('details.' . $loop->index . '.weight') }}">
                        {!! $errors->has('details.' . $loop->index . '.weight') ? '<p class="help-block" data-id="input-weight-" ' . $loop->index . '>'.$errors->first('details.' . $loop->index . '.weight').'</p>' : "" !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group {{ $errors->has('details.' . $loop->index . '.qty') ? 'has-error simple' : '' }}" data-id="{{ 'input-qty-' . $loop->index }}">
                    <div class="col-sm-12">
                        <input id="{{ 'input-qty-' . $loop->index }}" class="form-control qty-services" type="number" min="0" name="{{ 'details[' . $loop->index . '][qty]' }}" placeholder="{{ trans('page/transaction.delivery.field.detail.qty') }}" value="{{ old('details.' . $loop->index . '.qty') }}">
                        {!! $errors->has('details.' . $loop->index . '.qty') ? '<p class="help-block" data-id="input-qty-" ' . $loop->index . '>'.$errors->first('details.' . $loop->index . '.qty').'</p>' : "" !!}
                    </div>
                </div>
            </td>
            <td>
                <div class="form-group {{ $errors->has('details.' . $loop->index . '.subtotal') ? 'has-error simple' : '' }}">
                    <div class="col-sm-12">
                        <input id="{{ 'input-subtotal-' . $loop->index }}" class="form-control" type="number" min="0" name="{{ 'details[' . $loop->index . '][subtotal]' }}" placeholder="{{ trans('page/transaction.delivery.field.detail.subtotal') }}" value="{{ old('details.' . $loop->index . '.subtotal') }}" readonly>
                        {!! $errors->has('details.' . $loop->index . '.subtotal') ? '<p class="help-block" data-id="input-subtotal-" ' . $loop->index . '>'.$errors->first('details.' . $loop->index . '.subtotal').'</p>' : "" !!}
                    </div>
                </div>
            </td>
            <td>
                @if ($loop->first)
                    <button id="detail-add" type="button" class="btn btn-primary">{{ trans_choice('layout.buttons.add', 0) }}</button>
                @else
                    <button type="button" class="btn btn-danger detail-delete" data-id="{{ 'detail-' . $loop->index }}">{{ trans_choice('layout.buttons.delete', 0) }}</button>
                @endif
            </td>
        </tr>
    @endforeach
@else
    @php 
        $index = $index ?? 0
    @endphp
    <tr id="detail-{{ $index }}">
        <td>
            <div class="form-group" data-id="input-service-{{ $index }}">
                <div class="col-sm-12">
                    <select name="details[{{ $index }}][service]" id="input-service-{{ $index }}" class="form-control select-services">
                        <option disabled selected>{{ trans('page/transaction.delivery.field.detail.service') }}</option>
                        @foreach ($services as $service)
                            <option value="{{ $service->id }}" data-service='{!! json_encode($service) !!}'>{{ $service->fullname_display }}</option>
                        @endforeach
                    </select>
                    <input id="input-price-{{ $index }}" type="hidden" name="details[{{ $index }}][price]" value="" readonly>
                </div>
            </div>
        </td>
        <td>
            <div class="form-group" data-id="input-description-{{ $index }}">
                <div class="col-sm-12">
                    <input id="{{ 'input-description-' . $index }}" class="form-control" type="text" name="{{ 'details[' . $index . '][description]' }}" placeholder="{{ trans('page/transaction.delivery.field.detail.description') }}" value="{{ old('details.' . $index . '.description') }}">
                </div>
            </div>
        </td>
        <td>
            <div class="form-group" data-id="input-weight-{{ $index }}">
                <div class="col-sm-12">
                    <input id="{{ 'input-weight-' . $index }}" class="form-control weight-services" type="number" min="0" step="50" name="{{ 'details[' . $index . '][weight]' }}" placeholder="{{ trans('page/transaction.delivery.field.detail.weight') }}" value="{{ old('details.' . $index . '.weight') }}">
                </div>
            </div>
        </td>
        <td>
            <div class="form-group" data-id="input-qty-{{ $index }}">
                <div class="col-sm-12">
                    <input id="{{ 'input-qty-' . $index }}" class="form-control qty-services" type="number" min="0" name="{{ 'details[' . $index . '][qty]' }}" placeholder="{{ trans('page/transaction.delivery.field.detail.qty') }}" value="{{ old('details.' . $index . '.qty') }}">
                </div>
            </div>
        </td>
        <td>
            <div class="form-group">
                <div class="col-sm-12">
                    <input id="{{ 'input-subtotal-' . $index }}" class="form-control" type="number" min="0" name="{{ 'details[' . $index . '][subtotal]' }}" placeholder="{{ trans('page/transaction.delivery.field.detail.subtotal') }}" value="{{ old('details.' . $index . '.subtotal') }}" readonly>
                </div>
            </div>
        </td>
        <td>
            <button type="button" class="btn btn-danger detail-delete" data-id="detail-{{ $index }}">{{ trans_choice('layout.buttons.delete', 0) }}</button>
        </td>
    </tr>
@endif