<div id="form-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="modalHeading">{{ $modalTitle }}</h4>
            </div>
            {{ Form::open(['route' => $route, 'method' => $method, 'class' => 'form-horizontal', 'id'=>'form-section']) }}
                <div class="modal-body">
                    <div class="form-group {{ $errors->has('name') ? 'has-error simple' : '' }}" data-id="input-name">
                        <label for="input-name" class="col-sm-2 control-label">@lang('page/transaction.service.field.name')</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" value="{{ old('name', $servicedata ? $servicedata->name : null) }}" class="form-control" id="input-name" placeholder="@lang('page/transaction.service.field.name')">
                            {!! $errors->has("name") ? '<p class="help-block" data-id="input-name">'.$errors->first('name').'</p>' : "" !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('price') ? 'has-error simple' : '' }}" data-id="input-price">
                        <label for="input-price" class="col-sm-2 control-label">@lang('page/transaction.service.field.price')</label>
                        <div class="col-sm-8">
                            <input type="text" name="price" value="{{ old('price', $servicedata ? $servicedata->price : null) }}" class="form-control" id="input-price" placeholder="@lang('page/transaction.service.field.price')"> 
                            {!! $errors->has("price") ? '<p class="help-block" data-id="input-price">'.$errors->first('price').'</p>' : "" !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('status') ? 'has-error simple' : '' }}" data-id="input-status">
                        <label for="input-status" class="col-sm-2 control-label">@lang('page/transaction.service.field.status.title')</label>
                        <div class="col-sm-8">
                            {{ Form::select('status', $service_status, old('status', $servicedata ? $servicedata->status : null), [
                                'placeholder' => trans('page/transaction.service.field.status.title'),
                                'class' => 'form-control', 'id' => 'input-status'
                            ]) }}
                            {!! $errors->has("status") ? '<p class="help-block" data-id="input-status">'.$errors->first('status').'</p>' : "" !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{ trans_choice('layout.buttons.cancel', 0) }}</button>
                    <button type="submit" class="btn btn-primary">@lang('layout.buttons.save')</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@if (count($errors) > 0)
    @push('scripts')
        <script type="text/javascript">
            window.initPixel.push(function () {
                $('#form-modal').modal('show'); 
            });
        </script>
    @endpush 
@endif