@extends('site.layouts.app')
@section('content')
    @component('site.layouts.page_header')
        @slot('title') {{ $title }} @endslot
        <div class="pull-right">
            <a class="btn btn-default" href="{{ url()->previous() != url()->current() ? url()->previous() : route('transaction.index') }}">
                @lang('layout.buttons.back')
            </a>
        </div>
    @endcomponent
    @includeWhen(session('flash_messages'), 'site.layouts.feedback')
    {{ Form::open([
        'route' => 'transaction.store',
        'class' => 'panel form-horizontal',
    ]) }}
        <div class="panel-heading">
            <span class="panel-title">@lang('page/transaction.delivery.text.form_data')</span>
        </div>
        <div class="panel-body">
            <div class="form-group {{ $errors->has('branch') ? 'has-error simple' : '' }}" data-id="input-branch">
                <label for="input-branch" class="col-sm-2 control-label" data-id="input-branch">@lang('page/transaction.delivery.field.branch')</label>
                <div class="col-sm-10">
                    {{ Form::select('branch', $branches, old('branches'), [
                        'placeholder' => trans('page/transaction.delivery.field.branch'),
                        'id' => 'input-branch',
                        'class' => 'form-control select2',
                    ]) }}
                    {!! $errors->has("branch") ? '<p class="help-block" data-id="input-branch">'.$errors->first('branch').'</p>' : "" !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('due_date') ? 'has-error simple' : '' }}" data-id="input-due_date">
                <label for="input-due_date" class="col-sm-2 control-label">@lang('page/transaction.delivery.field.due_date')</label>
                <div class="col-sm-4">
                    <div class="input-group date" id="datepicker-component">
                        <input type="text" name="due_date" value="{{ old('due_date') }}" class="form-control" id="input-due_date" placeholder="@lang('page/transaction.delivery.field.due_date')" autocomplete="off"> 
                        <span class="input-group-addon"><i class="fa fa-calendar"></i>
                    </div>
                    {!! $errors->has("due_date") ? '<p class="help-block" data-id="input-due_date">'.$errors->first('due_date').'</p>' : "" !!}
                </div>
            </div>
            <hr class="panel-wide">
            <div class="row">
                {{-- DATA BILL --}}  
                    <div class="col-md-6">
                        <h6 class="text-light-gray text-semibold text-xs">@lang('page/transaction.delivery.text.bill_data')</h6>
                        <div class="form-group {{ $errors->has('bill_name') ? 'has-error simple' : '' }}" data-id="input-bill_name">
                            <label for="input-bill_name" class="col-sm-4 control-label">@lang('page/transaction.delivery.field.bill_name')</label>
                            <div class="col-sm-8">
                                <input type="text" name="bill_name" value="{{ old('bill_name') }}" class="form-control" id="input-bill_name" placeholder="@lang('page/transaction.delivery.field.bill_name')">   
                                {!! $errors->has("bill_name") ? '<p class="help-block" data-id="input-bill_name">'.$errors->first('bill_name').'</p>' : "" !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('bill_address') ? 'has-error simple' : '' }}" data-id="input-bill_address">
                            <label for="input-bill_address" class="col-sm-4 control-label">@lang('page/transaction.delivery.field.bill_address')</label>
                            <div class="col-sm-8">
                                <textarea name="bill_address" id="input-bill_address" class="form-control" rows="3" placeholder="@lang('page/transaction.delivery.field.bill_address')">{{ old('bill_address') }}</textarea>
                                {!! $errors->has("bill_address") ? '<p class="help-block" data-id="input-bill_address">'.$errors->first('bill_address').'</p>' : "" !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('bill_phone') ? 'has-error simple' : '' }}" data-id="input-bill_phone">
                            <label for="input-bill_phone" class="col-sm-4 control-label">@lang('page/transaction.delivery.field.bill_phone')</label>
                            <div class="col-sm-8">
                                <input type="text" name="bill_phone" value="{{ old('bill_phone') }}" class="form-control" id="input-bill_phone" placeholder="@lang('page/transaction.delivery.field.bill_phone')">   
                                {!! $errors->has("bill_phone") ? '<p class="help-block" data-id="input-bill_phone">'.$errors->first('bill_phone').'</p>' : "" !!}
                            </div>
                        </div>
                    </div>
                {{-- END DATA BILL --}}
                {{-- DATA RECIEVER --}}
                    <div class="col-md-6">
                        <h6 class="text-light-gray text-semibold text-xs">@lang('page/transaction.delivery.text.reciever_data')</h6>
                        <div class="form-group {{ $errors->has('reciever_name') ? 'has-error simple' : '' }}" data-id="input-reciever_name">
                            <label for="input-reciever_name" class="col-sm-4 control-label">@lang('page/transaction.delivery.field.reciever_name')</label>
                            <div class="col-sm-8">
                                <input type="text" name="reciever_name" value="{{ old('reciever_name') }}" class="form-control" id="input-reciever_name" placeholder="@lang('page/transaction.delivery.field.reciever_name')">   
                                {!! $errors->has("reciever_name") ? '<p class="help-block" data-id="input-reciever_name">'.$errors->first('reciever_name').'</p>' : "" !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('reciever_address') ? 'has-error simple' : '' }}" data-id="input-reciever_address">
                            <label for="input-reciever_address" class="col-sm-4 control-label">@lang('page/transaction.delivery.field.reciever_address')</label>
                            <div class="col-sm-8">
                                <textarea name="reciever_address" id="input-reciever_address" class="form-control" rows="3" placeholder="@lang('page/transaction.delivery.field.reciever_address')">{{ old('reciever_address') }}</textarea>
                                {!! $errors->has("reciever_address") ? '<p class="help-block" data-id="input-reciever_address">'.$errors->first('reciever_address').'</p>' : "" !!}
                            </div>
                        </div>
                        <div class="form-group {{ $errors->has('reciever_phone') ? 'has-error simple' : '' }}" data-id="input-reciever_phone">
                            <label for="input-reciever_phone" class="col-sm-4 control-label">@lang('page/transaction.delivery.field.reciever_phone')</label>
                            <div class="col-sm-8">
                                <input type="text" name="reciever_phone" value="{{ old('reciever_phone') }}" class="form-control" id="input-reciever_phone" placeholder="@lang('page/transaction.delivery.field.reciever_phone')">   
                                {!! $errors->has("reciever_phone") ? '<p class="help-block" data-id="input-reciever_phone">'.$errors->first('reciever_phone').'</p>' : "" !!}
                            </div>
                        </div>
                    </div>
                {{-- END DATA RECIEVER --}}
            </div>
            {{-- DATA BARANG --}}
                <hr class="panel-wide">
                <h6 class="text-light-gray text-semibold text-xs">@lang('page/transaction.delivery.text.delivery_data')</h6>
                <div class="row table-responsive">
                    <div class="col-md-12">
                        <table id="list-details" class="table table-hover">
                            <thead>
                                <tr>
                                    <th style="width: 180px;">@lang('page/transaction.delivery.field.detail.service')</th>
                                    <th style="width: 180px;">@lang('page/transaction.delivery.field.detail.description')</th>
                                    <th style="width: 140px;">@lang('page/transaction.delivery.field.detail.weight')</th>
                                    <th style="width: 100px;">@lang('page/transaction.delivery.field.detail.qty')</th>
                                    <th style="width: 180px;">@lang('page/transaction.delivery.field.detail.subtotal')</th>
                                    <th style="width: 100px;">@lang('layout.text.action')</th>
                                </tr>
                            </thead>
                            @php
                                $details = old('details');
                                $_details = !empty($details) ? $details : [0];
                            @endphp
                            <tbody>
                                @include('site.pages.transaction.delivery_service_input', ['details' => $_details])
                            </tbody>
                        </table>
                    </div>
                </div>
            {{-- END DATA BARANG --}}
            <hr class="panel-wide">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('total_weight') ? 'has-error simple' : '' }}" data-id="input-total_weight">
                        <label for="input-total_weight" class="col-sm-4 control-label">@lang('page/transaction.delivery.field.total_weight')</label>
                        <div class="col-sm-8">
                            <input type="text" name="total_weight" value="{{ old('total_weight') }}" class="form-control" id="input-total_weight" placeholder="@lang('page/transaction.delivery.field.total_weight')" readonly>
                            {!! $errors->has("total_weight") ? '<p class="help-block" data-id="input-total_weight">'.$errors->first('total_weight').'</p>' : "" !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group {{ $errors->has('total') ? 'has-error simple' : '' }}" data-id="input-total">
                        <label for="input-total" class="col-sm-4 control-label">@lang('page/transaction.delivery.field.total')</label>
                        <div class="col-sm-8">
                            <input type="text" name="total" value="{{ old('total') }}" class="form-control" id="input-total" placeholder="@lang('page/transaction.delivery.field.total')" readonly>
                            {!! $errors->has("total") ? '<p class="help-block" data-id="input-total">'.$errors->first('total').'</p>' : "" !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer text-right">
            <button type="submit" class="btn btn-primary btn-loading" data-loading-text="{{ trans('layout.text.loading') }}">@lang('layout.buttons.save')</button>
        </div>
    {{ Form::close() }}
@endsection
@push('scripts')
    <script id="form-detail" type="text/template">
        @include('site.pages.transaction.delivery_service_input', ['index' => '${count}'])
    </script>
    <script type="text/javascript">
        function render(props) {
            return function(tok, i) { 
                return (i % 2) ? props[tok] : tok; 
            };
        }

        function formatRupiah(nominal = 0, format = 'Rp', symbol = false) {
            var rev = parseInt(nominal, 10).toString().split('').reverse().join('');
            var rev2 = '';
            for (var i = 0; i < rev.length; i++) {
                rev2 += rev[i];
                if ((i + 1) % 3 === 0 && i !== (rev.length - 1)) {
                    rev2 += '.';
                }
            }
            var res = rev2.split('').reverse().join('');
            return (symbol) ? format + ' ' + res : res;
        }

        function calculateDetails() {
            var count = $("#list-details > tbody > tr").length;
            var grandtotal = 0;
            var total_weight = 0;

            for (let index = 0; index < count; index++) {
                var subtotal = 0;

                var weight = $('#input-weight-' + index).val();
                const service = $('#input-service-' + index).find(':selected').data('service');
                var qty = $('#input-qty-' + index).val();

                if (service) {
                    $('#input-description-' + index).val(service.name);
                }
                if (service && qty && weight) {
                    $('#input-price-' + index).val(service.price);
                    subtotal = parseInt(qty) * parseFloat(service.price);
                    total_weight += (parseInt(qty) * parseInt(weight));
                }
                $('#input-subtotal-' + index).val(formatRupiah(subtotal));
                grandtotal += subtotal;
            }
            $('#input-total_weight').val(total_weight);
            $('#input-total').val(formatRupiah(grandtotal));
        }

        window.initPixel.push(function(){
            $('#datepicker-component').datepicker({
                format: 'yyyy-mm-dd',
            });

            var template = $('script[id="form-detail"]').text().split(/\$\{(.+?)\}/g);

            $(document).on('click', '#detail-add', function(){
                var count = $("#list-details > tbody > tr").length;
                $('#list-details > tbody:last-child').append(template.map(render({
                    count: count,
                })).join(''));
                calculateDetails();
            });

            $(document).on('click', '.detail-delete', function(){
                $('#' + $(this).data('id')).remove();
                calculateDetails();
            });

             $(document).on('change', '.select-services', function(){
                /* const btn = $(this);
                const service = btn.find(':selected').data('service');
                console.log(service); */
                calculateDetails();
            });

            $(document).on('keyup click', '.qty-services, .weight-services', function(){
                calculateDetails();
            });
        });
    </script>
@endpush