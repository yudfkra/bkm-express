@extends('site.layouts.app')
@section('content')
    @component('site.layouts.page_header')
        @slot('title') {{ $title }} @endslot
        <div class="pull-right">
            <button class="btn btn-primary" data-toggle="modal" data-target="#branch-modal">
                <i class="fa fa-plus"></i> @lang('page/branch.section.add')
            </button>
        </div>
    @endcomponent
    @includeWhen(session('flash_messages'), 'site.layouts.feedback')
    <div class="panel">
        <div class="panel-heading">
            <span class="panel-title">@lang('page/branch.description')</span>
        </div>
        <div class="panel-body with-margins">
            <div class="row">
                <div class="col-md-12">
                    <div class="table-primary table-responsive">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th style="width: 10px;">#</th>
                                    <th>@lang('page/branch.field.name')</th>
                                    <th>@lang('page/branch.field.agent')</th>
                                    <th>@lang('page/branch.field.address')</th>
                                    <th>@lang('page/branch.field.phone')</th>
                                    <th>@lang('page/branch.field.fax')</th>
                                    <th>@lang('page/branch.field.contact')</th>
                                    <th>@lang('page/branch.field.cellular')</th>
                                    <th style="width: 100px;">@lang('layout.text.action')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse ($branches as $branch)
                                    <tr>
                                        <td>{{ $offset++ }}</td>
                                        <td>
                                            <a href="{{ route('branch.detail', ['id' => $branch->id]) }}">{{ $branch->name }}</a>
                                        </td>
                                        <td>{{ $branch->agent->name }}</td>
                                        <td>{{ $branch->address }}</td>
                                        <td>{{ $branch->phone }}</td>
                                        <td>{{ $branch->fax }}</td>
                                        <td>{{ $branch->contact }}</td>
                                        <td>{{ $branch->cellular }}</td>
                                        <td>
                                            <a href="{{ route('branch.detail', ['id' => $branch->id]) }}" class="btn btn-primary" data-toggle="tooltip" title="{{ trans_choice('layout.buttons.detail', 0) }}" data-placement="top">
                                                <i class="fa fa-search"></i>
                                            </a>
                                            {{ Form::open([
                                                'method'=>'delete',
                                                'route' => ['branch.destroy', $branch->id],
                                                'style' => 'display:inline;'
                                            ]) }}
                                                <button type="button" class="btn btn-danger confirm-modal" data-message="{{ trans('page/branch.text.confirm_delete') }}"><i class="fa fa-trash-o" data-toggle="tooltip" title="{{ trans_choice('common.buttons.delete', 0) }}" data-placement="top"></i></button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td colspan="9" align="center">{{ trans('messages.errors.empty_data') }}</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                    {!! $branches->links() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@push('html')
    @include('site.pages.branch.modal', [
        'route' => 'branch.store',
        'method' => 'POST',
        'modalTitle' => trans('page/branch.section.add'),
        'branchdata' => false,
    ])
@endpush