@extends('site.layouts.app')
@section('content')
    @component('site.layouts.page_header')
        @slot('title') {{ $title }} @endslot
    @endcomponent
    @includeWhen(session('flash_messages'), 'site.layouts.feedback')
    @if (!empty($branch))
        <div class="row">
            <div class="col-md-10 col-xs-offset-1">
                <div class="panel">
                    <div class="panel-heading">
                        <span class="panel-title">
                            {{ trans_choice('page/branch.section.detail', 1, ['branch' => $branch->name]) }}
                        </span>
                    </div>
                    <div class="panel-body">
                        <table class="table table-hover">
                            <tr>
                                <th>@lang('page/branch.field.name')</th>
                                <td>{{ $branch->name }}</td>
                            </tr>
                            <tr>
                                <th>@lang('page/branch.field.agent')</th>
                                <td>{{ $branch->agent->name }}</td>
                            </tr>
                            <tr>
                                <th>@lang('page/branch.field.address')</th>
                                <td>{!! nl2br($branch->address) !!}</td>
                            </tr>
                            <tr>
                                <th>@lang('page/branch.field.phone')</th>
                                <td>{{ $branch->phone }}</td>
                            </tr>
                            <tr>
                                <th>@lang('page/branch.field.fax')</th>
                                <td>{{ $branch->fax }}</td>
                            </tr>
                            <tr>
                                <th>@lang('page/branch.field.contact')</th>
                                <td>{{ $branch->contact }}</td>
                            </tr>
                            <tr>
                                <th>@lang('page/branch.field.cellular')</th>
                                <td>{{ $branch->cellular }}</td>
                            </tr>
                        </table>
                    </div>
                    <div class="panel-footer">
                        <a href="{{ url()->previous() }}" class="btn btn-default">@lang('layout.buttons.back')</a>
                        {{ Form::open([
                            'method'=>'delete',
                            'route' => ['branch.destroy', $branch->id],
                            'class' => 'pull-right',
                            'style' => 'display:inline; margin-left: 2rem;'
                        ]) }}
                            <button type="button" class="btn btn-danger confirm-modal" data-message="{{ trans('page/branch.text.confirm_delete') }}"><i class="fa fa-trash-o"></i> {{ trans_choice('layout.buttons.delete', 0) }}</button>
                        {{ Form::close() }}
                        <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#branch-modal"><i class="fa fa-pencil"></i> {{ trans_choice('layout.buttons.edit', 0) }}</button>
                    </div>
                </div>
            </div>
        </div>
    @else
        @include('site.pages.notfound', ['route' => route('branch.index')])
    @endif
@endsection
@push('html')
    @includeWhen($branch, 'site.pages.branch.modal', [
        'route' => ['branch.update', $branch ? $branch->id : null],
        'method' => 'PUT',
        'modalTitle' => trans('page/branch.section.edit'),
        'branchdata' => $branch,
    ])
@endpush