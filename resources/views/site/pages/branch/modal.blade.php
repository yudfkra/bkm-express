<div id="branch-modal" class="modal fade" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="modalHeading">{{ $modalTitle }}</h4>
            </div>
            {{ Form::open(['route' => $route, 'method' => $method, 'class' => 'form-horizontal']) }}
                <div class="modal-body">
                    <div class="form-group {{ $errors->has('name') ? 'has-error simple' : '' }}">
                        <label for="input-name" class="col-sm-2 control-label">@lang('page/branch.field.name')</label>
                        <div class="col-sm-8">
                            <input type="text" name="name" value="{{ old('name', $branchdata ? $branchdata->name : null) }}" class="form-control" id="input-name" placeholder="@lang('page/branch.field.name')">
                            {!! $errors->has("name") ? '<p class="help-block" data-id="input-name">'.$errors->first('name').'</p>' : "" !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('agent_id') ? 'has-error simple' : '' }}">
                        <label for="input-agent_id" class="col-sm-2 control-label">@lang('page/branch.field.agent')</label>
                        <div class="col-sm-8">
                            {{ Form::select('agent_id', $agents, old('agent_id', $branchdata ? $branchdata->agent_id : null), [
                                'placeholder' => trans('page/branch.field.agent'),
                                'class' => 'form-control', 'id' => 'input-agent_id'
                            ]) }}
                            {!! $errors->has("agent_id") ? '<p class="help-block" data-id="input-agent_id">'.$errors->first('agent_id').'</p>' : "" !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('address') ? 'has-error simple' : '' }}">
                        <label for="input-address" class="col-sm-2 control-label">@lang('page/branch.field.address')</label>
                        <div class="col-sm-8">
                            <textarea id="input-address" class="form-control" name="address" rows="3" placeholder="@lang('page/branch.field.address')">{{ old('address', $branchdata ? $branchdata->address : null) }}</textarea>
                            {!! $errors->has("address") ? '<p class="help-block" data-id="input-address">'.$errors->first('address').'</p>' : "" !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('phone') ? 'has-error simple' : '' }}">
                        <label for="input-phone" class="col-sm-2 control-label">@lang('page/branch.field.phone')</label>
                        <div class="col-sm-8">
                            <input type="text" name="phone" value="{{ old('phone', $branchdata ? $branchdata->phone : null) }}" class="form-control" id="input-phone" placeholder="@lang('page/branch.field.phone')">
                            {!! $errors->has("phone") ? '<p class="help-block" data-id="input-name">'.$errors->first('phone').'</p>' : "" !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('fax') ? 'has-error simple' : '' }}">
                        <label for="input-fax" class="col-sm-2 control-label">@lang('page/branch.field.fax')</label>
                        <div class="col-sm-8">
                            <input type="text" name="fax" value="{{ old('fax', $branchdata ? $branchdata->fax : null) }}" class="form-control" id="input-fax" placeholder="@lang('page/branch.field.fax')">
                            {!! $errors->has("fax") ? '<p class="help-block" data-id="input-fax">'.$errors->first('fax').'</p>' : "" !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('contact') ? 'has-error simple' : '' }}">
                        <label for="input-contact" class="col-sm-2 control-label">@lang('page/branch.field.contact')</label>
                        <div class="col-sm-8">
                            <input type="text" name="contact" value="{{ old('contact', $branchdata ? $branchdata->contact : null) }}" class="form-control" id="input-contact" placeholder="@lang('page/branch.field.contact')">
                            {!! $errors->has("contact") ? '<p class="help-block" data-id="input-contact">'.$errors->first('contact').'</p>' : "" !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('cellular') ? 'has-error simple' : '' }}">
                        <label for="input-cellular" class="col-sm-2 control-label">@lang('page/branch.field.cellular')</label>
                        <div class="col-sm-8">
                            <input type="text" name="cellular" value="{{ old('cellular', $branchdata ? $branchdata->cellular : null) }}" class="form-control" id="input-cellular" placeholder="@lang('page/branch.field.cellular')">
                            {!! $errors->has("cellular") ? '<p class="help-block" data-id="input-cellular">'.$errors->first('cellular').'</p>' : "" !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{ trans_choice('layout.buttons.cancel', 0) }}</button>
                    <button type="submit" class="btn btn-primary">@lang('layout.buttons.save')</button>
                </div>
            {{ Form::close() }}
        </div>
    </div>
</div>
@if (count($errors) > 0)
@push('scripts')
    <script type="text/javascript">
        window.initPixel.push(function () {
            $('#branch-modal').modal('show'); 
        });
    </script>
@endpush @endif