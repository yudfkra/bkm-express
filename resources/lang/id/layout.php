<?php

return [
    'meta' => [
        'title' => '{0} :appname |{1} :title - :appname',
    ],
    'menu' => [
        'dashboard' => [
            'title' => 'Dashboard',
        ],
        'branch' => [
            'title' => 'Kantor Cabang',
        ],
        'transaction' => [
            'title' => 'Transaksi',
            'child' => [
                'delivery' => 'Pengiriman',
                'service' => 'Layanan',
            ],
        ]
    ],
    'buttons' => [
        'hide_menu' => 'Hide Menu',
        'back' => 'Kembali',
        'save' => 'Simpan',
        'add' => '{0} Tambah |{1} Tambah :Item',
        'detail' => '{0} Detail |{1} Detail :Item',
        'edit' => '{0} Edit |{1} Edit :Item',
        'delete' => '{0} Hapus |{1} Hapus :Item',
        'cancel' => '{0} Batal |{1} Batalkan :Item',
        'search' => '{0} Cari |{1} Cari :Item',
    ],
    'text' => [
        'action' => 'Aksi',
        'keyword' => 'Kata Kunci',
        'loading' => 'Memproses...',
    ]
];