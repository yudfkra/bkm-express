<?php

return [
    'errors' => [
        'title' => 'Terjadi kesalahan!',
        'empty_data' => 'Tidak ada data.',
        'not_found' => '{0} Data tidak ditemukan. |{1} :Item tidak ditemukan!',
        'back_or_search' => '<a href=":url">Kembali</a> atau cari data berdasarkan kata kunci.',
        'validation' => 'Validasi error!',
        'branch' => [
            'failed_create' => 'Gagal menambahkan data kantor cabang. silahkan coba lagi',
            'failed_delete' => 'Gagal menghapus data kantor cabang. silahkan coba lagi',
            'failed_update' => 'Gagal mengubah data kantor cabang. silahkan coba lagi',
        ],
        'service' => [
            'failed_delete' => 'Gagal menghapus data layanan. silahkan coba lagi',
            'failed_create' => 'Gagal menambahkan data layanan. silahkan coba lagi',
            'failed_update' => 'Gagal mengubah data layanan. silahkan coba lagi',
        ],
        'delivery' => [
            'already_success' => 'Transaksi pengiriman sudah dikonfirmasi.',
            'already_cancelled' => 'Transaksi pengiriman sudah dibatalkan.',
            'failed_create' => 'Gagal menambahkan data transaksi pengiriman. silahkan coba lagi.',
            'failed_update' => 'Gagal merubah data transaksi pengiriman',
        ]
    ],
    'success' => [
        'title' => 'Sukses!',
        'branch' => [
            'created' => 'Berhasil menambahkan data kantor cabang.',
            'deleted' => 'Kantor cabang berhasil dihapus.',
            'updated' => 'Data kantor cabang berhasil diubah.'
        ],
        'service' => [
            'created' => 'Berhasil menambahkan data layanan.',
            'deleted' => 'Layanan berhasil dihapus.',
            'updated' => 'Data layanan berhasil diubah.',
        ],
        'delivery' => [
            'created' => 'Berhasil menambahkan data transaksi pengiriman.',
            'updated' => 'Data transaksi pengiriman ":invoice" berhasil diubah.',
        ]
    ],
];