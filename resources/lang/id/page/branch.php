<?php

return [
    'title' => 'Kantor Cabang',
    'description' => 'Daftar Kantor Cabang',
    'section' => [
        'add' => 'Tambah Data',
        'detail' => '{0} Detail Kantor Cabang |{1} :Branch - Detail Kantor Cabang',
        'edit' => 'Edit Data',
    ],
    'field' =>  [
        'agent' => 'Agen',
        'name' => 'Nama',
        'address' => 'Alamat',
        'phone' => 'No. Telp',
        'fax' => 'Fax',
        'contact' => 'Kontak',
        'cellular' => 'Seluler',
    ],
    'text' => [
        'confirm_delete' => 'Anda yakin ingin menghapus Kantor Cabang ini ?',
    ],
];