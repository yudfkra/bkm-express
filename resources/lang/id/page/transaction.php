<?php

return [
    'title' => 'Transaksi',
    'description' => 'Menu Transaksi',
    'delivery' => [
        'title' => 'Pengiriman',
        'description' => 'Daftar Transaksi Pengiriman',
        'section' => [
            'add' => 'Tambah Pengiriman',
            'detail' => '{0} Detail Pengiriman |{1} :invoice - Detail Pengiriman',
        ],
        'field' => [
            'invoice' => 'Invoice',
            'receipt' => 'Kode Transaksi',
            'branch' => 'Kantor Cabang',
            'bill_name' => 'Pengirim',
            'bill_address' => 'Alamat Pengirim',
            'bill_phone' => 'No. Pengirim',
            'reciever_name' => 'Penerima',
            'reciever_address' => 'Alamat Penerima',
            'reciever_phone' => 'No. Penerima',
            'total_weight' => 'Total Berat (Gram)',
            'total' => 'Total',
            'status' => [
                'title' => 'Status',
                'cancelled' => 'Dibatalkan',
                'success' => 'Sukses',
                'process' => 'Proses',
            ],
            'due_date' => 'Jatuh Tempo',
            'created_at' => 'Tanggal Transaksi',
            'updated_at' => 'Perubahan Terakhir',
            'detail' => [
                'service' => 'Layanan',
                'description' => 'Deskripsi',
                'qty' => 'Qty',
                'weight' => 'Berat (Gram)',
                'price' => 'Harga Unit',
                'subtotal' => 'Sub Total',
            ]
        ],
        'text' => [
            'form_data' => 'Form Pengiriman',
            'delivery_data' => 'Data Barang',
            'bill_data' => 'Data Pengirim',
            'reciever_data' => 'Data Penerima',
            'print' => 'Cetak',
            'confirm_success' => 'Anda yakin ingin mengkonfirmasi pengiriman ini ?',
            'confirm_cancelled' => 'Anda yakin ingin membatalkan Pengiriman ini ?',
        ],
        'buttons' => [
            'success' => 'Konfirmasi Pengiriman',
            'cancelled' => 'Batalkan Pengiriman',
        ]
    ],
    'service' => [
        'title' => 'Layanan',
        'description' => 'Daftar Layanan',
        'section' => [
            'add' => 'Tambah Data',
            'edit' => 'Edit Data',
        ],
        'field' => [
            'name' => 'Nama Layanan',
            'price' => 'Harga',
            'status' => [
                'title' => 'Status Layanan',
                'active' => 'Aktif',
                'inactive' => 'Tidak Aktif',
            ],
        ],
        'text' => [
            'confirm_delete' => 'Anda yakin ingin menghapus layanan ini ?',
        ],
    ],
];