<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['auth.sso']], function () {
    Route::get('/', function () {
        return view('welcome');
    })->name('index');

   Route::group(['prefix' => 'branch', 'as' => 'branch.'], function () {
        Route::delete('destroy/{id}', 'BranchController@destroyBranch')->name('destroy');
        Route::put('detail/{id?}', 'BranchController@updateBranch')->name('update');
        Route::get('detail/{id?}', 'BranchController@showBranch')->name('detail');
        Route::post('store', 'BranchController@storeBranch')->name('store');
        Route::get('/', 'BranchController@showBranchList')->name('index'); 
    });

    Route::group(['prefix' => 'transaction', 'as' => 'transaction.'], function () {
        Route::group(['prefix' => 'service', 'as' => 'service.'], function () {
            Route::delete('destroy/{id}', 'TransactionController@destroyService')->name('destroy');
            Route::put('update/{id}', 'TransactionController@updateService')->name('update');
            Route::post('store', 'TransactionController@storeService')->name('store');
            Route::get('/', 'TransactionController@showServices')->name('index'); 
        });

        Route::post('store', 'TransactionController@storeDelivery')->name('store');
        Route::get('create', 'TransactionController@showDeliveryForm')->name('create');
        Route::patch('/{id}/action', 'TransactionController@handleDeliveryAction')->name('action');
        Route::get('/{id}/print', 'TransactionController@printDelivery')->name('print');
        Route::get('/{id}', 'TransactionController@showDelivery')->name('show');
        Route::get('/', 'TransactionController@index')->name('index');
    }); 
});