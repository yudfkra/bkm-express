<?php

return [
    'host_url' => env('BKM_HOST_URL', 'http://localhost:8000/server'),
    'broker_id' => env('BKM_BROKER_ID', 7155928),
    'broker_secret' => env('BKM_BROKER_SECRET', '23db9c258cd459c2c38ee7119cb28c246644190f'),
    'uri' => [
        'profile' => 'profile',
        'login' => 'login',
        'logout' => 'logout',
    ]
];